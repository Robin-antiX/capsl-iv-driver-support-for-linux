# CAPSL-IV printer driver support for linux

Make CAPSL-IV printers print in high quality (600 dpi) from within linux, allowing a clear and sharp printout.

All this is about identifying the control sequences needed, by analysing the output of multiple printouts from within Windows 2000 using the driver option "print to file".

All the sequences already identifed are found within the folder "Identified Control Sequences", available in German and English language both.

Watch sample files within "CAPSL-IV Testprints" folder using a hex editor. They are named by the different available printing options used on creating them. They are meant as a reference for the strings described in the "identified control sequences" folder. All identified sequences were derived from this kind of printouts.

## Authors and acknowledgment
This is an [antiX linux](https://www.antixforum.com) community project.

## License
GPL v3

## Project status
in progress

