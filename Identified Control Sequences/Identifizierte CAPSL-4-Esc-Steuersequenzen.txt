Durch Inaugenscheinnahme zweier Geräte läßt sich feststellen: Der "LBP-8 Mark IV" hat das identische 600 dpi Druckwerk wie ein HP Laserjet 4 Drucker (HP hat die Druckwerke für seine Laserdrucker seinerzeit bei Cannon eingekauft), aber der 8-IV versteht die "HP-PCL" Sprache zur Übertragung nicht. Er erwartet stattdessen "CAPSL-IV".
Unter Linux ist es bisher nötig, diese Dateien unter einem virtualisierten Windows 2000 zu erzeugen, und sie dann aus Linux per dd an den Drucker zu senden, um ein vernünftiges Druckergebnis zu erhalten. Das ist ein unhaltbarer Zustand.
Die Steuersequenzen habe ich durch vergleichende Analyse aus einer Vielzahl gedruckter Dateien gewonnen, die mit der Option "print to file" in Windows 2000 mit dem Originaltreiber gedruckt wurden (zunächst, um sie dann aus dem Linux-System an den daran angeschlossenen Drucker schicken zu können). 
Daher hier der Versuch, die Steuersequenzen zu identifizieren, um einen funktionierenden Linux-Treiber daraus zu erstellen.

Die vorgefundenen Daten weisen eine sehr einfache Struktur auf:

ESC steht für das Steuerzeichen "hex 1b"

Jedes an den Drucker geschickte Dokument wird mit der folgenden Sequenz eingeleitet:
Dokumentenbeginn =	ESC%@ESCP40JESC\ESC[1"|ESC[0"pESC<ESC[7 IESC[11hESC[?32hESC[?1lESC[?2hESC[?8l

Dann folgt immer die Seqzenz zum Setzen der Dokumentausrichtung: Hochformat oder Querformat:

Hochformat setzen =	ESC[0%r
Querformat setzen =	ESC[1%r

Die nächste identifizierte Sequenz in den Druckdateien ist die Kopienanzahl:

gewünschte Anzahl der Kopien =	ESC[NUMv   (Mit NUM = Anzahl, z.B.:)
  1  Kopie  = ESC[1v
  2  Kopien = ESC[2v
  46 Kopien = ESC[46v
  usw.
(Es scheint eine Begrenzung auf 99 Exemplare zu geben, denn in der capsl-Datei steht maximal ESC[99v, auch wenn in Windows 2000 z.B. 114 oder 130 Exemplare in das Druckmenü eingetragen werden)

gefolgt von der Auswahlsequenz für den Papiereinzug

Papierschacht manuell =	ESC[1q
Papiereinzug unten =	ESC[2q
Papiereinzug oben =	ESC[3q
Briefumschlageinzug =	ESC[4q

Als nächstes wird das Papierformat übermittelt.

Einzelblätter:
Blattformat DIN A4 (Hochformat) setzen =	ESC[14;;p
Blattformat DIN A5 (Hochformat) setzen =	ESC[16;;p
Blattformat DIN B5 (Hochformat) setzen =	ESC[26;;p
Blattformat DIN A4 (Querformat) setzen =	ESC[15;;p
Blattformat DIN A5 (Querformat) setzen =	ESC[17;;p
Blattformat DIN B5 (Querformat) setzen =	ESC[27;;p

oder Briefumschläge: 
"Briefumschlag DIN-lang"=	
"Briefumschlag B5" =		
"Briefumschlag C5" =		

(Es gibt weitere (amerikanische) Blattformate, sind noch nicht analysiert:)
"Executive"
"Legal"
"Letter"
"Briefumschlag Monarch"

Nun wird der "Zeichenstift" auf dem Blatt positioniert:
Positioniere Stift horizontal relativ bei =	ESC[NUMa	(mit NUM = Abstand, Bewegung nach rechts, in Zeichnungseinheiten)
Positioniere Stift vertikal absolut bei =	ESC[NUMd	(Mit NUM = Abstand von der Nullinie, in Zeichnungseinheiten)
Positioniere Stift vertikal relativ =		ESC[NUMe	(Mit NUM = Abstand, Bewegung nach unten, in Zeichnungseinheiten)
Positioniere Stift vertikal relativ =		ESC[NUMk	(Mit NUM = Abstand, Bewegung nach oben, in Zeichnungseinheiten)

Und dann werden die Rasterdaten Blockweise übertragen:

Zeichnungsdaten senden 300 dpi =	ESC[NUM;NUM;300;11;NUM;;;;;.r
Zeichnungsdaten senden 600 dpi =	ESC[NUM;NUM;600;11;NUM;;;;;.r

Die NUM - Werte geben Aufschluß über die Struktur der Rasterdaten:
Der erste NUM Wert gibt offenbar die Gesamtlänge der Rasterdaten in Bytes an, die dem Befehl folgen.
Der zweite NUM Wert scheint die Zeilenlänge des Ausschnittes in Bytes auszuweisen,
Der dritte NUM Wert korreliert immer mit der Höhe des von den Rasterdaten beschriebenen Zeichnungsausschnittes in Zeichnungseinheiten.


Die Befehlssequenzen zum "Positionieren des Stiftes horizontal", "vertikal" und "Zeichnungsdaten senden" wiederholen sich innerhalb eines Dokumentes dann so oft, bis alle Zeichnungsdaten dargestellt sind.

Am Dokumentende, direkt nach den letzten Rasterdaten, wird immer die Sequenz gesendet:

Dokumentende =		ESC[1vESC[0#xESCP0JESC\



Zusatzinfo:
Der Drucker kann lt. altem Herstellerprospekt mit 2MB, 3MB, 4MB, 6MB, 7MB, 10MB und 12MB internem RAM ausgestattet sein. Davon hängt es ab, wieviele Rasterdaten in einem Rutsch verarbeitet werden können.

Standardzeichensätze, die ich durch den integrierten Druckerselbsttest ausgegeben bekomme, sind:
"Courier", "Elite", "Swiss Dutch", "Symbol", jeweils in 10 und 12 cpi.

Außerdem gibt es Geräte mit eingebauten Zeichensatzkartuschen. Das wird beim Rasterdruck aber nicht benötigt.

Nichtbedruckbare Blattränder lt. Druckerhandbuch:
  oben:   5 mm
  links:  4 mm
  rechts: 8 mm
  unten:  5 mm

Der Halbtonalgorithmus bei der Rasterdatenerzeugung sollte passend zu der Auflösung gewählt werden. Die Abstufungen sind unter Linux viel zu grob. Aus Windows 2000 heraus lassen sich sogar Fotos in brauchbarer Schwarzweißqualität (600dpi) drucken! In Linux kommen nur ein paar Balken dabei heraus. Das liegt daran, daß der falsche HP-Drucker mit schlechter Rasterung und zu geringer verfügbarer Auflösung als Basis für die vorhandenen CAPSL Linuxtreiber verwendet wurde.




